package com.mobilejazz.doc.domain.interactor.character;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.SettableFuture;
import com.mobilejazz.core.common.Callback;
import com.mobilejazz.core.datasource.Repository;
import com.mobilejazz.core.datasource.spec.IdentifierRepositorySpecification;
import com.mobilejazz.core.datasource.spec.RepositorySpecification;
import com.mobilejazz.doc.domain.model.Character;

import javax.inject.Inject;

public class GetCharacterInteractor {

  private final ListeningExecutorService executorService;
  private final Repository<Character, RepositorySpecification> characterRepository;

  @Inject public GetCharacterInteractor(ListeningExecutorService executorService,
      Repository<Character, RepositorySpecification> characterRepository) {
    this.executorService = executorService;
    this.characterRepository = characterRepository;
  }

  public ListenableFuture<Character> execute(final String identifier) {
    final SettableFuture<Character> settableFuture = SettableFuture.create();

    executorService.execute(new Runnable() {
      @Override public void run() {
        RepositorySpecification specification = new IdentifierRepositorySpecification(identifier);

        characterRepository.get(specification, new Callback<Character>() {
          @Override public void onSuccess(Character character) {
            settableFuture.set(character);
          }

          @Override public void onError(Throwable throwable) {
            settableFuture.setException(throwable);
          }
        });
      }
    });

    return settableFuture;
  }
}
