package com.mobilejazz.doc.domain.interactor.character;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.SettableFuture;
import com.mobilejazz.core.common.Callback;
import com.mobilejazz.core.datasource.Repository;
import com.mobilejazz.core.datasource.spec.RepositorySpecification;
import com.mobilejazz.doc.domain.model.Character;

import javax.inject.Inject;
import java.util.*;

public class GetCharactersInteractor {

  private final ListeningExecutorService executorService;
  private final Repository<Character, RepositorySpecification> characterRepository;

  @Inject public GetCharactersInteractor(ListeningExecutorService executorService,
      Repository<Character, RepositorySpecification> characterRepository) {
    this.executorService = executorService;
    this.characterRepository = characterRepository;
  }

  public ListenableFuture<List<Character>> execute() {
    final SettableFuture<List<Character>> settableFuture = SettableFuture.create();

    executorService.execute(new Runnable() {
      @Override public void run() {
        characterRepository.getAll(new Callback<List<Character>>() {
          @Override public void onSuccess(List<Character> characters) {
            settableFuture.set(characters);
          }

          @Override public void onError(Throwable throwable) {
            settableFuture.setException(throwable);
          }
        });
      }
    });

    return settableFuture;
  }
}
