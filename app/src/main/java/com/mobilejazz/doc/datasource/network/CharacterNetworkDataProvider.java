package com.mobilejazz.doc.datasource.network;

import com.mobilejazz.core.common.Callback;
import com.mobilejazz.core.datasource.Repository;
import com.mobilejazz.core.datasource.spec.RepositorySpecification;
import com.mobilejazz.doc.datasource.model.CharacterEntity;

import java.util.*;

public class CharacterNetworkDataProvider
    implements Repository.Network<CharacterEntity, RepositorySpecification> {

  @Override public void get(Callback<CharacterEntity> callback) {

  }

  @Override public void getAll(Callback<List<CharacterEntity>> callback) {

  }

  @Override
  public void get(RepositorySpecification specification, Callback<CharacterEntity> callback) {

  }

  @Override public void getAll(RepositorySpecification specification,
      Callback<List<CharacterEntity>> callback) {

  }

  @Override public void put(CharacterEntity characterEntity, Callback<CharacterEntity> callback) {

  }

  @Override public void putAll(List<CharacterEntity> characterEntities,
      Callback<List<CharacterEntity>> callback) {

  }

  @Override public void put(CharacterEntity characterEntity, RepositorySpecification specification,
      Callback<CharacterEntity> callback) {

  }

  @Override
  public void putAll(List<CharacterEntity> characterEntities, RepositorySpecification specification,
      Callback<List<CharacterEntity>> callback) {

  }

  @Override
  public void remove(CharacterEntity characterEntity, Callback<CharacterEntity> callback) {

  }

  @Override public void removeAll(List<CharacterEntity> characterEntities,
      Callback<List<CharacterEntity>> callback) {

  }

  @Override
  public void remove(CharacterEntity characterEntity, RepositorySpecification specification,
      Callback<CharacterEntity> callback) {

  }

  @Override public void removeAll(List<CharacterEntity> characterEntities,
      RepositorySpecification specification, Callback<List<CharacterEntity>> callback) {

  }
}
