package com.mobilejazz.doc.datasource;

import com.mobilejazz.core.common.Callback;
import com.mobilejazz.core.datasource.Repository;
import com.mobilejazz.core.datasource.mapper.Mapper;
import com.mobilejazz.core.datasource.spec.RepositorySpecification;
import com.mobilejazz.doc.datasource.model.CharacterEntity;
import com.mobilejazz.doc.domain.model.Character;

import javax.inject.Inject;
import java.util.*;

public class CharacterDataProvider implements Repository<Character, RepositorySpecification> {

  private final Mapper<Character, CharacterEntity> toCharacterEntity;
  private final Mapper<List<CharacterEntity>, List<Character>> toCharacterList;
  private final Mapper<CharacterEntity, Character> toCharacter;
  private final Repository.Network<CharacterEntity, RepositorySpecification> characterNetwork;

  @Inject public CharacterDataProvider(Mapper<Character, CharacterEntity> toCharacterEntity,
      Mapper<List<CharacterEntity>, List<Character>> toCharacterList,
      Mapper<CharacterEntity, Character> toCharacter,
      Network<CharacterEntity, RepositorySpecification> characterNetwork) {
    this.toCharacterEntity = toCharacterEntity;
    this.toCharacterList = toCharacterList;
    this.toCharacter = toCharacter;
    this.characterNetwork = characterNetwork;
  }

  @Override public void get(Callback<Character> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void getAll(Callback<List<Character>> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override
  public void get(RepositorySpecification specification, final Callback<Character> callback) {
    characterNetwork.get(specification, new Callback<CharacterEntity>() {
      @Override public void onSuccess(CharacterEntity characterEntity) {
        Character character = toCharacter.map(characterEntity);

        if (callback != null) {
          callback.onSuccess(character);
        }
      }

      @Override public void onError(Throwable throwable) {
        if (callback != null) {
          callback.onError(throwable);
        }
      }
    });
  }

  @Override public void getAll(RepositorySpecification specification,
      final Callback<List<Character>> callback) {
    characterNetwork.getAll(specification, new Callback<List<CharacterEntity>>() {
      @Override public void onSuccess(List<CharacterEntity> characterEntities) {
        List<Character> characters = toCharacterList.map(characterEntities);

        if (callback != null) {
          callback.onSuccess(characters);
        }
      }

      @Override public void onError(Throwable throwable) {
        if (callback != null) {
          callback.onError(throwable);
        }
      }
    });
  }

  @Override public void put(Character character, Callback<Character> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void putAll(List<Character> characters, Callback<List<Character>> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void put(Character character, RepositorySpecification specification,
      Callback<Character> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void putAll(List<Character> characters, RepositorySpecification specification,
      Callback<List<Character>> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void remove(Character character, Callback<Character> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void removeAll(List<Character> characters, Callback<List<Character>> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void remove(Character character, RepositorySpecification specification,
      Callback<Character> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }

  @Override public void removeAll(List<Character> characters, RepositorySpecification specification,
      Callback<List<Character>> callback) {
    throw new UnsupportedOperationException("Operation not supported");
  }
}
