package com.mobilejazz.doc.application.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.mobilejazz.core.domain.interactor.InteractorHandler;
import com.mobilejazz.doc.R;
import com.mobilejazz.doc.domain.interactor.character.GetCharacterInteractor;
import com.mobilejazz.doc.domain.interactor.character.GetCharactersInteractor;
import com.mobilejazz.doc.domain.model.Character;

import javax.inject.Inject;
import java.util.*;

public class MainActivity extends AppCompatActivity {

  @Inject GetCharactersInteractor getCharactersInteractor;
  @Inject GetCharacterInteractor getCharacterInteractor;
  @Inject InteractorHandler interactorHandler;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // Request all the characters
    ListenableFuture<List<Character>> getCharactersListenableFuture =
        getCharactersInteractor.execute();

    // Add the future callback that return the value in Main Thread
    interactorHandler.addCallbackMainThread(getCharactersListenableFuture,
        new FutureCallback<List<Character>>() {
          @Override public void onSuccess(List<Character> result) {
            // Display the characters
          }

          @Override public void onFailure(Throwable t) {
            // Nothing to do
          }
        });

    ListenableFuture<Character> characterOneFuture =
        getCharacterInteractor.execute("fake.identifier.one");
    ListenableFuture<Character> characterTwoFuture =
        getCharacterInteractor.execute("fake.identifier.two");

    ListenableFuture<List<Character>> charatersDetail =
        Futures.allAsList(characterOneFuture, characterTwoFuture);

    interactorHandler.addCallback(charatersDetail, new FutureCallback<List<Character>>() {
      @Override public void onSuccess(List<Character> result) {
        // Wrap the values in a List<Character>
      }

      @Override public void onFailure(Throwable t) {
        // Nothing to do
      }
    });

  }
}
